/**
 * Indicates that this module contains classes that need to be generated / processed.
 */
@ModuleGen(name = "subscription-microservices", groupPackage = "com.amazonservices.mws.subscriptions")

package com.amazonservices.mws.subscriptions;

import io.vertx.codegen.annotations.ModuleGen;