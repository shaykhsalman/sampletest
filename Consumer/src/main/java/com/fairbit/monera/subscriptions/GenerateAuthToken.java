package com.fairbit.monera.subscriptions;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class GenerateAuthToken {
    public String getToken(){
        HttpResponse<JsonNode> stringHttpResponse = null;
        try {
            stringHttpResponse = Unirest.post("https://fairbit.auth0.com/oauth/token")
                    .header("content-type", "application/json")
                    .body("{\"grant_type\":\"client_credentials\",\"client_id\": \"OdYFooFK1mDtMSoefVTv7CZEZEV5sB68\",\"client_secret\": \"2xSE7bPTwBSZ_pnv2edGX8cc7A4S7bTzZ9FVeL5NhQBBs7KJBj0-zS0D4fVdMOWm\",\"audience\": \"https://fairbit.auth0.com/api/v2/\"}")
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }


       return (String) stringHttpResponse.getBody().getObject().get("access_token");
    }
}
