package com.fairbit.monera.subscriptions;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

import java.io.IOException;

public interface SendGridAPI {
    public void sendEmail(Long user_id, String email, String temp_pass, Handler<AsyncResult<String>> resultHandler) throws IOException;
}
