package com.fairbit.monera.subscriptions;

import com.sendgrid.*;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.io.IOException;

public class SendGridImpl implements SendGridAPI {

    private static final Logger logger = LoggerFactory.getLogger(SendGridImpl.class);

    @Override
    public void sendEmail(Long user_id, String email, String temp_pass, Handler<AsyncResult<String>> resultHandler) throws IOException {
        Email from = new Email("info@fairbit.com");
        String subject = "Welcome To Fairbit! Confirm Your Email";
        Email to = new Email("iamsksalman@gmail.com");
        Content content = new Content("text/html", "Your Temporary Password is: "+temp_pass);
        Mail mail = new Mail(from, subject, to, content);

        SendGrid sg = new SendGrid("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");
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            if(response.getStatusCode() == 202){
                logger.info("Mail Sent Successfully to + "+email);
                resultHandler.handle(Future.succeededFuture());
            }else{
               logger.error("Failed to send Email.... ");
                resultHandler.handle(Future.failedFuture("Failed to Send Email...."));
            }
        } catch (Exception ex) {
            resultHandler.handle(Future.failedFuture(ex.getMessage()));
            throw ex;
        }
    }
}
