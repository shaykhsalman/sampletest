package com.fairbit.monera.subscriptions;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageBatchResultEntry;

import java.util.List;

public interface SqsAPI {
    public final static String accesskey = "AKIAJSJ54EG2EICHJXQQ";
    public final static String privateKey = "UTd9yrP33aDQmms2QtbEjFi/ZajSadWtSjX5yY+c";


    public  AmazonSQS getConnection();
    public List<String> getQueues(AmazonSQS amazonSQS);
    public  String createQueue(AmazonSQS amazonSQS, String myqueue);
    public  String sendMessageRequest(AmazonSQS amazonSQS, String queueUrl, String msg);
    public  List<SendMessageBatchResultEntry> sendMultipleMessageRequest(AmazonSQS amazonSQS, String queueUrl, List<String> msgs);
    public  List<Message> receiveMessage(String myQueueUrl, AmazonSQS amazonSQS);
    public  String deleteMessage(Message messages, AmazonSQS amazonSQS, String myQueueUrl);
    public  void closeConnection(AmazonSQS amazonSQS);
}
