package com.fairbit.monera.subscriptions;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * SQS API to create, delete, send, and read message
 *
 */
public class SqsAPIimpl implements SqsAPI{

    private final static String accesskey = "AKIAIVHF3YVYPOZH4QTA";
    private final static String privateKey = "UTd9yrP33aDQmms2QtbEjFi/ZajSadWtSjX5yY+c";

    public  AmazonSQS getConnection(){

            AmazonSQS amazonSQS;
            AWSCredentials awsCredentials = new BasicAWSCredentials(accesskey, privateKey);
            Region region = Region.getRegion(Regions.US_EAST_2);
            amazonSQS = AmazonSQSClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                    .withRegion(region.getName())
                    .build();
        return amazonSQS;
    }

    public  String createQueue(AmazonSQS amazonSQS, String myqueue){
        CreateQueueRequest createQueueRequest = new CreateQueueRequest().withQueueName(myqueue);
        return amazonSQS.createQueue(createQueueRequest).getQueueUrl();
    }
    public  List<String> getQueues(AmazonSQS amazonSQS){
        List<String> queues = new ArrayList<String>();
        for (String queueUrl : amazonSQS.listQueues().getQueueUrls()) {
            queues.add(queueUrl);
        }
        return queues;
    }
    public  String sendMessageRequest(AmazonSQS amazonSQS, String queueUrl, String msg){
        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(msg)
                .withDelaySeconds(0);
        SendMessageResult sendMessageResult = amazonSQS.sendMessage(send_msg_request);
        return sendMessageResult.getMessageId();

    }
    public  List<SendMessageBatchResultEntry> sendMultipleMessageRequest(AmazonSQS amazonSQS, String queueUrl, List<String> msgs){
        SendMessageBatchRequest send_batch_request = new SendMessageBatchRequest()
                .withQueueUrl(queueUrl)
                .withEntries(
                        new SendMessageBatchRequestEntry(
                                "msg_1", "Hello from message 1"),
                        new SendMessageBatchRequestEntry(
                                "msg_2", "Hello from message 2")
                                .withDelaySeconds(10));
        SendMessageBatchResult sendMessageBatchRequest = amazonSQS.sendMessageBatch(send_batch_request);
        return sendMessageBatchRequest.getSuccessful();
    }
    public  List<Message> receiveMessage(String myQueueUrl, AmazonSQS amazonSQS){
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(myQueueUrl);
        receiveMessageRequest.setMaxNumberOfMessages(10);
        List<Message> messages = amazonSQS.receiveMessage(receiveMessageRequest).getMessages();
        amazonSQS = null;
        return messages;
    }
    public  String deleteMessage(Message messages, AmazonSQS amazonSQS, String myQueueUrl){
        String messageReceiptHandle = messages.getReceiptHandle();
        DeleteMessageResult deleteMessageResult = amazonSQS.deleteMessage(new DeleteMessageRequest()
                .withQueueUrl(myQueueUrl)
                .withReceiptHandle(messageReceiptHandle));
        return deleteMessageResult.toString();
    }
    public  void closeConnection(AmazonSQS amazonSQS){
        amazonSQS.shutdown();
    }
    public static void main(String[] args) {
        SqsAPIimpl sqsAPIimpl = new SqsAPIimpl();
        AmazonSQS amazonSQS = sqsAPIimpl.getConnection();
//
//
        List<String> queues = sqsAPIimpl.getQueues(amazonSQS);
        for(String queue: queues){
            System.out.println("QUEUE URL: "+queue);
        }
//
////        sendMessageRequest(amazonSQS ,queues.get(0), "Hello from abc");
//
        List<Message> messages = sqsAPIimpl.receiveMessage(queues.get(1), amazonSQS);
        for(Message message: messages){
            System.out.println(message.getBody());
        }
//        closeConnection(amazonSQS);
    }
}
