package com.fairbit.monera.subscriptions;

import io.vertx.core.Future;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.fairbit.monera.commonservices.RestAPIVerticle;

public class SubscriptionsAPIverticle extends RestAPIVerticle {


    private final SubscriptionsCRUDService service;
    public SubscriptionsAPIverticle(SubscriptionsCRUDService service) {
        this.service = service;
    }
    //default
    private static int role = 100;
    private static long merchant_id;
    private static final Logger logger = LoggerFactory.getLogger(SubscriptionsAPIverticle.class);

    private static final String API_CREATEMERCHANT = "/create-merchant/:userId";
    private static final String SERVICE_NAME = "subscriptions-rest-api";
    private static final String API_REGISTER = "/subscriptions";
    private static final String API_RETRIEVE = "/merchants/:merchant_id";
    private static final String API_DEREGISTER = "/deregister";
    private static final String API_USERSIGNUP = "/user/signup/";
    private static final String API_REGISTERMERCHANT = "/verify_credentials";
    public void start(Future<Void> future) throws Exception {
        super.start();

        Router router = Router.router(vertx);
        // body handler
        router.route().handler(BodyHandler.create());
        router.get("/test").handler(this::test);
        // API route handler
//        router.post(API_REGISTERMERCHANT).handler(this::apiRegisterMerchant);
//        router.post(API_REGISTER).handler(this::apiRegisterDestination);
//        router.get(API_RETRIEVE).handler(this::apiRetrieveDestination);
//        router.delete(API_DEREGISTER).handler(this::apiDeregisterDestination);
//        router.get(API_CREATEMERCHANT).handler(this::apiCreateMerchant);

        //user email signup
//        router.post(API_USERSIGNUP).handler(this::apiUserEmailSignUP);


        String host = "sample-hazelcast-consumer";
        int port = 8080;
        createHttpServer(router, host, port)
                .compose(serverCreated -> publishHttpEndpoint(SERVICE_NAME, host, port, "subscription"))
                .setHandler(future.completer());
    }

    private void test(RoutingContext context) {
        context.response().end("Subscription verticle..");
    }

}
