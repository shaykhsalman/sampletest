package com.fairbit.monera.subscriptions;


import com.amazonservices.mws.subscriptions.MWSSubscriptionsService;
import com.amazonservices.mws.subscriptions.model.*;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public  interface SubscriptionsCRUDService {

    String SERVICE_NAME = "archer-subscriptions-microservice";

    String SERVICE_ADDRESS = "archer-subscriptions-microservice";

    /**
     *
     * @param client         store object
     * @param request async result handler
     */
    public RegisterDestinationResponse RegisterDestination(MWSSubscriptionsService client, RegisterDestinationInput request);
    public ListRegisteredDestinationsResponse ListRegisteredDestinations(MWSSubscriptionsService client, ListRegisteredDestinationsInput request);
    public DeregisterDestinationResponse DeregisterDestination(MWSSubscriptionsService client, DeregisterDestinationInput request);

    public GetServiceStatusResponse GetServiceStatus(MWSSubscriptionsService client, GetServiceStatusRequest request);

    public CreateSubscriptionResponse CreateSubscription(MWSSubscriptionsService client, CreateSubscriptionInput request);
    public DeleteSubscriptionResponse DeleteSubscription(MWSSubscriptionsService client, DeleteSubscriptionInput request);
    public GetSubscriptionResponse GetSubscription(MWSSubscriptionsService client, GetSubscriptionInput request);
    public ListSubscriptionsResponse ListSubscriptions(MWSSubscriptionsService client, ListSubscriptionsInput request);
    public UpdateSubscriptionResponse UpdateSubscription(MWSSubscriptionsService client, UpdateSubscriptionInput request);
    public SendTestNotificationToDestinationResponse SendTestNotificationToDestination(MWSSubscriptionsService client, SendTestNotificationToDestinationInput request);

    public void registerMerchant(JsonObject data, Handler<AsyncResult<Void>> resultHandler);
    public void updateMerchant(JsonObject data, Handler<AsyncResult<Void>> resultHandler);
    public void createMerchant(String data, Handler<AsyncResult<Long>> resultHandler);
    public void registerUser(String data, Handler<AsyncResult<Integer>> resultHandler);

}
