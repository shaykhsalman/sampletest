package com.fairbit.monera.subscriptions;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonservices.mws.subscriptions.MWSSubscriptionsService;
import com.amazonservices.mws.subscriptions.MWSSubscriptionsServiceClient;
import com.amazonservices.mws.subscriptions.MWSSubscriptionsServiceException;
import com.amazonservices.mws.subscriptions.model.*;
import com.amazonservices.mws.subscriptions.samples.CreateSubscriptionSample;
import com.amazonservices.mws.subscriptions.samples.MWSSubscriptionsServiceSampleConfig;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.asyncsql.PostgreSQLClient;
import io.vertx.ext.sql.SQLClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/***
 * @author shaikhsalman
 * @version 1.0.1.Beta
 */
public class SubscriptionsCRUDServiceImpl implements SubscriptionsCRUDService {


    private static final Logger logger = LoggerFactory.getLogger(SubscriptionsCRUDServiceImpl.class);
    private SQLClient sqlClient;
    private static String error_msg;

    public SubscriptionsCRUDServiceImpl(){

    }

    /**
     *
     * @param vertx
     * @param config
     * @apiNote Constructor to instantiate Postgres database connection.
     */
    public SubscriptionsCRUDServiceImpl(Vertx vertx, JsonObject config){
    //Database Connection will be implemented here.
        JsonObject postgreSQLClientConfig = new JsonObject()
                .put("host", "moneradbserver-test.cuwbhrbzgqk9.us-east-1.rds.amazonaws.com")
                .put("username", "moneradbuser")
                .put("password", "Fb2017!!")
                .put("port", 5432)
                .put("database", "moneradb");
        this.sqlClient = PostgreSQLClient.createShared(vertx, postgreSQLClientConfig);
    }

    @Override
    public RegisterDestinationResponse RegisterDestination(
            MWSSubscriptionsService client,
            RegisterDestinationInput request) {
        try {
            // Call the service.
            RegisterDestinationResponse response = client.registerDestination(request);

            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MWSSubscriptionsServiceException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            RegisterDestinationResponse response = null;
            return response;
        }
    }
    @Override
    public  ListRegisteredDestinationsResponse ListRegisteredDestinations(
            MWSSubscriptionsService client,
            ListRegisteredDestinationsInput request) {
        try {
            // Call the service.
            ListRegisteredDestinationsResponse response = client.listRegisteredDestinations(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: " + rhmd.getRequestId());
            System.out.println("Timestamp: " + rhmd.getTimestamp());
            ListRegisteredDestinationsResult listRegisteredDestinationsResult = response.getListRegisteredDestinationsResult();
            DestinationList destinationList = listRegisteredDestinationsResult.getDestinationList();
//            List<Destination> members = destinationList.getMember();
//            for (Destination member : members) {
//                System.out.println("Delivery channel: " + member.getDeliveryChannel());
//                AttributeKeyValueList attributeList = member.getAttributeList();
//                List<AttributeKeyValue> member1 = attributeList.getMember();
//                for (AttributeKeyValue keyValue : member1) {
//                    System.out.println("Key " + keyValue.getKey());
//                    System.out.println(keyValue.getKey());
//                }
//            }
//            String responseXml = response.toXML();
//            System.out.println(responseXml);
            return response;
        } catch (MWSSubscriptionsServiceException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if (rhmd != null) {
                System.out.println("RequestId: " + rhmd.getRequestId());
                System.out.println("Timestamp: " + rhmd.getTimestamp());
            }
            System.out.println("Message: " + ex.getMessage());
            System.out.println("StatusCode: " + ex.getStatusCode());
            System.out.println("ErrorCode: " + ex.getErrorCode());
            System.out.println("ErrorType: " + ex.getErrorType());
            throw ex;
        }
    }
    @Override
    public  DeregisterDestinationResponse DeregisterDestination(
            MWSSubscriptionsService client,
            DeregisterDestinationInput request) {
        try {
            // Call the service.
            DeregisterDestinationResponse response = client.deregisterDestination(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MWSSubscriptionsServiceException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }
    @Override
    public  CreateSubscriptionResponse CreateSubscription(
            MWSSubscriptionsService client,
            CreateSubscriptionInput request) {
        try {
            // Call the service.
            CreateSubscriptionResponse response = client.createSubscription(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MWSSubscriptionsServiceException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }
    @Override
    public  DeleteSubscriptionResponse DeleteSubscription(
            MWSSubscriptionsService client,
            DeleteSubscriptionInput request) {
        try {
            // Call the service.
            DeleteSubscriptionResponse response = client.deleteSubscription(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MWSSubscriptionsServiceException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }
    @Override
    public  GetServiceStatusResponse GetServiceStatus(
            MWSSubscriptionsService client,
            GetServiceStatusRequest request) {
        try {
            // Call the service.
            GetServiceStatusResponse response = client.getServiceStatus(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MWSSubscriptionsServiceException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }
    @Override
    public GetSubscriptionResponse GetSubscription(
            MWSSubscriptionsService client,
            GetSubscriptionInput request) {
        try {
            // Call the service.
            GetSubscriptionResponse response = client.getSubscription(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MWSSubscriptionsServiceException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }
    @Override
    public ListSubscriptionsResponse ListSubscriptions(
            MWSSubscriptionsService client,
            ListSubscriptionsInput request) {
        try {
            // Call the service.
            ListSubscriptionsResponse response = client.listSubscriptions(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MWSSubscriptionsServiceException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }
    @Override
    public UpdateSubscriptionResponse UpdateSubscription(
            MWSSubscriptionsService client,
            UpdateSubscriptionInput request) {
        try {
            // Call the service.
            UpdateSubscriptionResponse response = client.updateSubscription(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MWSSubscriptionsServiceException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }
    @Override
    public SendTestNotificationToDestinationResponse SendTestNotificationToDestination(
            MWSSubscriptionsService client,
            SendTestNotificationToDestinationInput request) {
        try {
            // Call the service.
            SendTestNotificationToDestinationResponse response = client.sendTestNotificationToDestination(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MWSSubscriptionsServiceException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }

    @Override
    public void registerMerchant(JsonObject data, Handler<AsyncResult<Void>> resultHandler) {

        MWSSubscriptionsServiceClient client = MWSSubscriptionsServiceSampleConfig.getClient();

        final RegisterDestinationInput request = new RegisterDestinationInput();

        String user_id = data.getString("user_id");
        String sellerId = data.getString("sellerId");
        request.setSellerId(sellerId);

        String mwsAuthToken = data.getString("authToken");
        request.setMWSAuthToken(mwsAuthToken);

        String marketplaceId = data.getString("marketplace");
        request.setMarketplaceId(marketplaceId);


        Destination destination = new Destination();
        destination.setDeliveryChannel("SQS");
        SqsAPI sqsAPI = new SqsAPIimpl();

        AmazonSQS amsqs = sqsAPI.getConnection();

        String queue = sqsAPI.createQueue(amsqs, sellerId);

        AttributeKeyValue attributeKeyValue = new AttributeKeyValue();
        attributeKeyValue.setKey("sqsQueueUrl");
        attributeKeyValue.setValue(queue);

        List<AttributeKeyValue> objects  = new ArrayList<AttributeKeyValue>();
        objects.add(attributeKeyValue);

        AttributeKeyValueList attributeKeyValueList = new AttributeKeyValueList();
        attributeKeyValueList.setMember(objects);

        destination.setAttributeList(attributeKeyValueList);

        request.setDestination(destination);

        RegisterDestinationResponse registerDestinationResponse = RegisterDestination(client, request);
        if(registerDestinationResponse != null) {
            if (registerDestinationResponse.isSetResponseHeaderMetadata()) {

                logger.info("Registered Destination: " + queue);


                /*
                Creating subscription for the registered destination
                 */

                logger.info("Creating subscription for sellerID: " + sellerId);

                CreateSubscriptionInput requests = new CreateSubscriptionInput();
                requests.setSellerId(sellerId);
                requests.setMarketplaceId(marketplaceId);
                requests.setMWSAuthToken(mwsAuthToken);
                Subscription subscription = new Subscription();
                subscription.setDestination(destination);

                subscription.setNotificationType("AnyOfferChanged");

                subscription.setIsEnabled(true);
                requests.setSubscription(subscription);

                // Make the call.
                CreateSubscriptionSample.invokeCreateSubscription(client, requests);

                String query = "insert into merchantqueues(sellerid, marketplace, authtoken, sqsQueue, user_id) values(?,?,?,?, ?)";
                JsonArray params = new JsonArray().add(sellerId).add(marketplaceId).add(mwsAuthToken).add(queue).add(user_id);
                sqlClient.updateWithParams(query, params, updateResultAsyncResult -> {
                    if (updateResultAsyncResult.succeeded()) {
                        logger.info("Updated merchant Queue Information....");
                        resultHandler.handle(Future.succeededFuture());
                    } else {
                        logger.error("Failed to insert merchant information. Cause:  " + updateResultAsyncResult.cause().getMessage());
                        resultHandler.handle(Future.failedFuture(updateResultAsyncResult.cause()));
                    }
                });
//                db.addQueue(sellerId, mwsAuthToken, marketplaceId, queue);
                resultHandler.handle(Future.succeededFuture());
//                rc.response().setStatusCode(200).setStatusMessage("registered").end();

            } else {
                logger.error("Failed.....");
                resultHandler.handle(Future.failedFuture("Already exists.."));
            }
        }else{
            logger.error("************Invalid credentials************");
            resultHandler.handle(Future.failedFuture("Invalid Credentials..."));
        }
    }


    /**
     *
     * @param data
     * @param resultHandler
     * @apiNote To update merchant information into merchant table.
     * @since 12/01/2018 5:31PM
     */
    @Override
    public void updateMerchant(JsonObject data, Handler<AsyncResult<Void>> resultHandler) {
        String query = "update merchants set email = ?  where merchant_id = ?";
        sqlClient.updateWithParams(query, new JsonArray().add(data.getString("email")).add(data.getJsonObject("app_metadata").getLong("merchant_id")), result->{
            if(result.succeeded()){
                logger.info("Successfully updated email and name.....");
                resultHandler.handle(Future.succeededFuture());
            }else{
                resultHandler.handle(Future.failedFuture(result.cause()));
                logger.error(result.cause());
            }
        });
    }

    /**
     *
     * @param data
     * @param resultHandler
     * @apiNote To create merchant_id from user_id with creation time
     */
    @Override
    public void createMerchant(String data, Handler<AsyncResult<Long>> resultHandler) {
        String query = "insert into merchants(user_id, creation_time) values (?,now()) returning merchant_id";
        sqlClient.queryWithParams(query, new JsonArray().add(data), resultHandlr->{
           if(resultHandlr.succeeded()){
               long merchant_id = resultHandlr.result().getRows().get(0).getLong("merchant_id");
                logger.info("Successfuslly inserted and created merchant data....");
               resultHandler.handle(Future.succeededFuture(merchant_id));
           } else{
               resultHandler.handle(Future.failedFuture(resultHandlr.cause()));
               logger.error(resultHandlr.cause());
           }
        });
    }

    @Override
    public void registerUser(String data, Handler<AsyncResult<Integer>> resultHandler) {
        logger.info(data);
        SendGridAPI emailAPI = new SendGridImpl();
        JsonObject user_data = new JsonObject();
        String query = "insert into users(email, creation_time, temp_pass) values (?, now(), encrypt('"+data+"', 'F@irb!t@Email', 'aes')) returning user_id, temp_pass";
        sqlClient.queryWithParams(query, new JsonArray().add(data), ar->{
           if(ar.succeeded()){
               Long user_id = ar.result().getRows().get(0).getLong("user_id");
               String temp_pass = ar.result().getRows().get(0).getBinary("temp_pass").toString();
                logger.info("Successfully created temporary password and inserted it into database...");
               try {
                       String accessToken = new GenerateAuthToken().getToken();
                       user_data.put("connection", "Username-Password-Authentication").put("email", data).put("password", temp_pass).put("email_verified", true).put("verify_email", true);
                       HttpResponse<JsonNode> authorization = Unirest.post("https://fairbit.auth0.com/api/v2/users")
                               .header("content-type", "application/json")
                               .header("Authorization", "Bearer " + accessToken)
                               .body(user_data.toString())
                               .asJson();
                   System.out.println(authorization.getStatus());
                       if(authorization.getStatus() == 201){
                           emailAPI.sendEmail(user_id, data, temp_pass, result->{
                               if(result.succeeded()){
                                   resultHandler.handle(Future.succeededFuture(authorization.getStatus()));
                                   logger.info("Mail Sent Successfully...");
                               }else{
                                   logger.error("Failed to send Mail");
                                   resultHandler.handle(Future.failedFuture(authorization.getStatusText()));
                               }
                           });
                       }else{
                           logger.error("Failed to create user in Auth0");
                           resultHandler.handle(Future.failedFuture(authorization.getStatusText()));
                       }
               } catch (IOException e) {
                   e.printStackTrace();
               } catch (UnirestException e) {
                   e.printStackTrace();
               }
           } else{
                logger.error("Failed to insert & create temporary password "+ar.cause());
                resultHandler.handle(Future.failedFuture(ar.cause()));
           }
        });

    }

}
