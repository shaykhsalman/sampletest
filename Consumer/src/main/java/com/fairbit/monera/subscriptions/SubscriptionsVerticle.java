package com.fairbit.monera.subscriptions;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import org.fairbit.monera.commonservices.BaseVerticle;

import static com.fairbit.monera.subscriptions.SubscriptionsCRUDService.SERVICE_ADDRESS;
import static com.fairbit.monera.subscriptions.SubscriptionsCRUDService.SERVICE_NAME;

/**
 * Hello world!
 *
 */
public class SubscriptionsVerticle extends BaseVerticle {
    private SubscriptionsCRUDService subscriptionsCRUDService;


    @Override
    public void start(Future<Void> startFuture) throws Exception {
        JsonObject config = new JsonObject();
        subscriptionsCRUDService = new SubscriptionsCRUDServiceImpl(vertx, config);

        publishEventBusService(SERVICE_NAME, SERVICE_ADDRESS, SubscriptionsCRUDService.class)
                .compose(servicePublished -> deployRestVerticle(subscriptionsCRUDService))
                .setHandler(startFuture.completer());
    }
    private Future<Void> deployRestVerticle(SubscriptionsCRUDService service) {
        Future<String> future = Future.future();
        vertx.deployVerticle(new SubscriptionsAPIverticle(service),
                new DeploymentOptions().setConfig(config()),
                future.completer());
        return future.map(r -> null);

    }
}
