/**
 * Indicates that this module contains classes that need to be generated / processed.
 */
@ModuleGen(name = "Consumer", groupPackage = "com.fairbit.monera.subscriptions")

package com.fairbit.monera.subscriptions;

import io.vertx.codegen.annotations.ModuleGen;