package org.publisher;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.impl.ConcurrentHashSet;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.KeyStoreOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.auth.jwt.JWTOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import io.vertx.servicediscovery.types.HttpEndpoint;
import org.fairbit.monera.commonservices.RestAPIVerticle;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Hello world!
 *
 */
public class App extends RestAPIVerticle {
    private static final int port = 8080;
    protected ServiceDiscovery serviceDiscovery;
    protected Set<Record> registeredRecords = new ConcurrentHashSet<>();
    private static final Logger logger = LoggerFactory.getLogger(App.class);


    @Override
    public void start(Future<Void> future) throws Exception {

        Router router = Router.router(vertx);

        JWTAuthOptions config = new JWTAuthOptions()
                .setKeyStore(new KeyStoreOptions()
                        .setPath("keystore.jceks")
                        .setPassword("secret"));
        JWTAuth jwt = JWTAuth.create(vertx, config);

        String host = "localhost";



        router.route("/").handler(this::home);

        // cookie and session handler


        // body handler
        router.route().handler(BodyHandler.create());



        // version handler

        router.get("/api/*").handler(JWTAuthHandler.create(jwt, "/api/login"));

        router.get("/api/login").handler(ctx -> {
            ctx.response().putHeader("Content-Type", "text/plain");
            ctx.response().end(jwt.generateToken(new JsonObject().put("username", "salman"), new JWTOptions()));
        });

        // create OAuth 2 instance for Keycloak



        // set auth callback handler


        // api dispatcher
        router.route("/api/auth/*").handler(this::dispatchRequests);

        // static content
//        router.route("/*").handler(StaticHandler.create());

        // enable HTTPS
        HttpServerOptions httpServerOptions = new HttpServerOptions();
//                .setSsl(true)
//                .setKeyStoreOptions(new JksOptions().setPath("server.jks").setPassword("123456"));

        // create http server
        vertx.createHttpServer(httpServerOptions)
                .requestHandler(router::accept)
                .listen(port, ar -> {
                    if (ar.succeeded()) {
                        publishApiGateway(host, port);
                        future.complete();
                        logger.info("API Gateway is running on port " + port + "host "+host);
                        // publish log
                        publishGatewayLog("api_gateway_init_success:" + port);

                        // publish log
                    } else {
                        future.fail(ar.cause());
                    }
                });




        serviceDiscovery = ServiceDiscovery.create(vertx,
                new ServiceDiscoveryOptions().setBackendConfiguration(new JsonObject()));

//        Record record = HttpEndpoint.createRecord("api", "localhost", 8080, "/",
//                new JsonObject().put("api.name", "test-endpoint"));
//
//
//
//
//        serviceDiscovery.publish(record, ar -> {
//            System.out.println(record.toJson());
//            if (ar.succeeded()) {
//                System.out.println("Service <" + ar.result().getName() + "> published. ");
//            } else {
//                System.out.println("Failed to publish"+ar.cause());
//            }
//        });
    }

    private void dispatchRequests(RoutingContext context) {
        int initialOffset = 10; // length of `/api/auth/`
        // run with circuit breaker in order to deal with failure
        logger.info("Dispatching request to services...");
        circuitBreaker.execute(future -> {
            getAllEndpoints().setHandler(ar -> {
                if (ar.succeeded()) {
                    List<Record> recordList = ar.result();
                    System.out.println(recordList.size());
                    // get relative path and retrieve prefix to dispatch client
                    String path = context.request().uri();

                    if (path.length() <= initialOffset) {
                        notFound(context);
                        future.complete();
                        return;
                    }
//                    System.out.println(recordList.get(0).getMetadata().toString());

                    String prefix = (path.substring(initialOffset)
                            .split("/"))[0];
                    // generate new relative path
                    String newPath = path.substring(initialOffset + prefix.length());
//                    System.out.println(newPath+ " ::: "+context);
                    // get one relevant HTTP client, may not exist
//                    System.out.println(prefix);
                    Optional<Record> client = recordList.stream()
                            .filter(record -> record.getMetadata().getString("api.name") != null)
                            .filter(record -> record.getMetadata().getString("api.name").equals(prefix))
                            .findAny(); // simple load balance
//                    System.out.println("API NAME"+client.get().getMetadata().getString("api.name"));
                    if (client.isPresent()) {
                        logger.info("Found service.."+client.get().getMetadata());
                        doDispatch(context, newPath, serviceDiscovery.getReference(client.get()).get(), future);
                    } else {
                        logger.info("No Service found..");
                        notFound(context);
                        future.complete();
                    }
                } else {
                    future.fail(ar.cause());
                }
            });
        }).setHandler(ar -> {
            if (ar.failed()) {
                badGateway(ar.cause(), context);
            }
        });
    }
    /**
     * Dispatch the request to the downstream REST layers.
     *
     * @param context routing context instance
     * @param path    relative path
     * @param client  relevant HTTP client
     */
    private void doDispatch(RoutingContext context, String path, HttpClient client, Future<Object> cbFuture) {
        HttpClientRequest toReq = client
                .request(context.request().method(), path, response -> {
                    response.bodyHandler(body -> {
                        if (response.statusCode() >= 500) { // api endpoint server error, circuit breaker should fail
                            cbFuture.fail(response.statusCode() + ": " + body.toString());
                        } else {
                            HttpServerResponse toRsp = context.response()
                                    .setStatusCode(response.statusCode());
                            response.headers().forEach(header -> {
                                toRsp.putHeader(header.getKey(), header.getValue());
                            });
                            // send response
                            toRsp.end(body);
                            cbFuture.complete();
                        }
                        ServiceDiscovery.releaseServiceObject(serviceDiscovery, client);
                    });
                });
        System.out.println(toReq.absoluteURI());
        // set headers
        context.request().headers().forEach(header -> {
            toReq.putHeader(header.getKey(), header.getValue());
        });
        if (context.user() != null) {
            toReq.putHeader("user-principal", context.user().principal().encode());
        }
        // send request
        if (context.getBody() == null) {
            toReq.end();
        } else {
            toReq.end(context.getBody());
        }
    }

    private void home(RoutingContext routingContext) {
        routingContext.response().end("Home Page");
    }




    @Override
    public void stop(Future<Void> stopFuture) throws Exception {
        List<Future> futures = new ArrayList<>();
        registeredRecords.forEach(record -> {
            Future<Void> cleanupFuture = Future.future();
            futures.add(cleanupFuture);
            serviceDiscovery.unpublish(record.getRegistration(), cleanupFuture.completer());
        });

        if (futures.isEmpty()) {
            serviceDiscovery.close();
            stopFuture.complete();
        } else {
            CompositeFuture.all(futures)
                    .setHandler(ar -> {
                        serviceDiscovery.close();
                        if (ar.failed()) {
                            stopFuture.fail(ar.cause());
                        } else {
                            stopFuture.complete();
                        }
                    });
        }
    }
    /**
     * Get all REST endpoints from the service discovery infrastructure.
     *
     * @return async result
     */
    private Future<List<Record>> getAllEndpoints() {
        Future<List<Record>> future = Future.future();
        serviceDiscovery.getRecords(record -> record.getType().equals(HttpEndpoint.TYPE),
                future.completer());
        serviceDiscovery.getRecord(record -> true, recordAsyncResult -> {
            if(recordAsyncResult.succeeded()){
                    logger.info("Found..");
                    logger.info(recordAsyncResult.result());
                    logger.info("Metadata :"+recordAsyncResult.result().getMetadata());
                System.out.println(recordAsyncResult.result().getMetadata());
                System.out.println("Data : "+recordAsyncResult.result().toJson());
            }else{
                System.out.println("No data..");
            }
        });
        return future;
    }

    // log methods

    private void publishGatewayLog(String info) {
        JsonObject message = new JsonObject()
                .put("info", info)
                .put("time", System.currentTimeMillis());
        publishLogEvent("gateway", message);
    }

    private void publishGatewayLog(JsonObject msg) {
        JsonObject message = msg.copy()
                .put("time", System.currentTimeMillis());
        publishLogEvent("gateway", message);
    }

}
