package com.fairbit.monera.subscriptions;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import io.vertx.servicediscovery.types.HttpEndpoint;

/**
 * Hello world!
 *
 */
public class App extends AbstractVerticle{
    @Override
    public void start(Future<Void> startFuture) throws Exception {
        ServiceDiscovery serviceDiscovery = ServiceDiscovery.create(vertx,
                new ServiceDiscoveryOptions().setBackendConfiguration(new JsonObject()));
        Record record = HttpEndpoint.createRecord("api", "localhost", 8080, "/",
                new JsonObject().put("api.name", "api-name"));
        serviceDiscovery.publish(record, ar -> {
            System.out.println(record.toJson());
            if (ar.succeeded()) {
                System.out.println("Service <" + ar.result().getName() + "> published. ");
            } else {
                System.out.println("Failed to publish"+ar.cause());
            }
        });
    }
}
